package search.google.steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import search.google.pages.GooglePage;

import java.util.List;

import static org.fest.assertions.Assertions.assertThat;

public class GoogleSteps {

    private final GooglePage googlePage = new GooglePage();

    @Given("user opens google.pl")
    public void openPage() {
        googlePage.open();
        googlePage.closeNotification();
    }

    @When("user searches for the phrase {string} in google")
    public void userSearchesInGoogle(final String searchedPhrase) {
        googlePage.search(searchedPhrase);
    }

    @When("user opens the first search result")
    public void userOpensTheFirstSearchResult() {
        googlePage.clickFirstResult();
    }

    @When("user opens Google Apps")
    public void userOpensGoogleApps() {
        googlePage.opensGoogleApps();
    }

    @Then("there are '{int}' searched results displayed on the page")
    public void searchResultsNumber(int expectedResultsNumber) {
        assertThat(googlePage.getAllSearchResults().size()).isEqualTo(expectedResultsNumber);
    }

    @Then("check Google Apps contains special components")
    public void checkGoogleAppsContainsSpecialComponents() {
        assertThat(googlePage.getSpecialComponentsNames())
                .contains("Account", "Search", "Maps", "YouTube", "Play", "News", "Gmail", "Meet", "Keep", "Jamboard",
                        "Earth", "Collections", "Arts and Culture", "Google Ads", "Podcasts", "Stadia", "Travel", "Forms");
    }

    @And("user check upper right component names")
    public void userCheckUpperRightComponentNames() {
        assertThat(googlePage.getUpperRightComponentNames()).contains("Gmail", "Images", "Sign in");
    }
}
