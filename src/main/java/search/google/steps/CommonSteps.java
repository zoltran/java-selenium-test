package search.google.steps;

import io.cucumber.java.en.Then;

import static com.codeborne.selenide.WebDriverRunner.url;
import static org.fest.assertions.Assertions.assertThat;

public class CommonSteps {

    @Then("url contains {string}")
    public void urlContains(final String expectedUrl) {
        assertThat(url()).contains(expectedUrl);
    }
}
