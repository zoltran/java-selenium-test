package search.google.steps;

import io.cucumber.java.en.Then;
import search.google.pages.FintecoPage;

import static org.fest.assertions.Assertions.assertThat;

public class FintecoPageSteps {

    private final FintecoPage fintecoPage = new FintecoPage();

    @Then("ProService Finteco Home page is displayed")
    public void proServiceFintecoHomePageIsDisplayed() {
        assertThat(fintecoPage.isDisplayed()).isTrue();
    }

}
