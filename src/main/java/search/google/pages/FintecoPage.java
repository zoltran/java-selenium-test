package search.google.pages;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class FintecoPage {

    private static final By FINTECO_HOME_PAGE = By.cssSelector("body.psfinteco");

    public boolean isDisplayed() {
        return $(FINTECO_HOME_PAGE).isDisplayed();
    }

}
