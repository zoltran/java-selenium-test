package search.google.pages;

import com.codeborne.selenide.*;
import com.netflix.config.DynamicProperty;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class GooglePage {

    private final static By NOTIFICATION = By.cssSelector("button[id='L2AGLb']");
    private final static By SEARCH_WINDOW = By.cssSelector("[name='q']");
    private final static By SEARCH_RESULT = By.cssSelector("[id='search'] a[href] h3");
    private final static By COMPONENTS_TOP_RIGHT = By.cssSelector("div[data-ogsr-up] [target='_top']");
    private final static By GOOGLE_APPS = By.cssSelector("a[aria-label='Google apps']");
    private final static By SPECIAL_COMPONENTS = By.cssSelector("ul a[target='_top']");
    private final static By IFRAME_WINDOW = By.cssSelector("iframe[src*='https://ogs.google.pl/widget/app']");
    private final static By LANGUAGE_BROWSER_EN = By.cssSelector("html[lang*='en']");


    String searchUrl = DynamicProperty.getInstance("service.url").getString();

    public void open() {
        Selenide.open(searchUrl);
        if (!$(LANGUAGE_BROWSER_EN).isDisplayed()){
            executeJavaScript("window.location.href = 'en';");
        }
    }

    public void closeNotification() {
        if ($(NOTIFICATION).isDisplayed()) {
            $(NOTIFICATION).click();
        }
    }

    public void search(String searchedPhrase) {
        $(SEARCH_WINDOW).sendKeys(searchedPhrase);
        $(SEARCH_WINDOW).pressEnter();
    }

    public void clickFirstResult() {
        $(SEARCH_RESULT).click();
    }

    public List<SelenideElement> getAllSearchResults() {
        return $$(SEARCH_RESULT);
    }

    public List<String> getUpperRightComponentNames() {
        List<SelenideElement> list = $$(COMPONENTS_TOP_RIGHT);
        return list
                .stream()
                .map(SelenideElement::text)
                .collect(Collectors.toList());
    }

    public void opensGoogleApps() {
        $(GOOGLE_APPS).click();
    }

    public List<String> getSpecialComponentsNames() {
        switchToFrame();
        List<SelenideElement> list = $$(SPECIAL_COMPONENTS);
        return list
                .stream()
                .map(SelenideElement::text)
                .collect(Collectors.toList());
    }

    private void switchToFrame() {
        getWebDriver()
                .switchTo()
                .frame($(IFRAME_WINDOW));
    }
}
