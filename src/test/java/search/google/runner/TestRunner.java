package search.google.runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"pretty", "html:target/cucumber-reports"},
        features = "src/test/resources/features/",
        glue = "search.google.steps"
)
public class TestRunner extends AbstractTestNGCucumberTests {
}