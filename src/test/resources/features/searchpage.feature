Feature: Google search engine tests

  @search-single-page
  Scenario: Searched page is displayed
    Given user opens google.pl
    When user searches for the phrase 'ProService Finteco' in google
    And user opens the first search result
    Then url contains 'psfinteco.com'
    And ProService Finteco Home page is displayed

  @search-results
  Scenario: Number of search results
    Given user opens google.pl
    When user searches for the phrase 'ProService Finteco' in google
    Then there are '10' searched results displayed on the page

  @google-home-page
  Scenario: Components on the Google home page
    Given user opens google.pl
    Then url contains 'google.pl'
    And user check upper right component names

  @google-components
  Scenario: Special Components on the Google home page
    Given user opens google.pl
    When user opens Google Apps
    Then check Google Apps contains special components
