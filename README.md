## Technologies
Project is created with:
* Java 11
* Apache Maven 3.6

## Setup
To run this project without tests, install it locally using mvn:
```
mvn clean install -DskipTests
```

## Tests
To run only tests:
```
mvn test
```

To run test with tag:
```
mvn test -Dcucumber.filter.tags="@search-results"
```